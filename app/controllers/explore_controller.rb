class ExploreController < ApplicationController

  # GET /tags
  # GET /tags.json
  def index
    @feeds = Feed.all.order_by(:title.asc)

    @groups = Hash.new

    @feeds.each do |feed|
      feed.tags.each do |tag|
        if @groups.include? tag
          @groups[tag].push(feed.title)
        else
          @groups[tag]=[feed.title]          
        end
      end
    end
    
    @groups = @groups.sort

    render layout: "application"
  end

end
