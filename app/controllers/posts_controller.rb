class PostsController < ApplicationController
skip_before_filter :verify_authenticity_token

  def index
    puts "++++++++++++++++++++++"

    #PARAMS ( page, tag, published)
    #page number default
    params[:page]  ||=1
    #published default
    @today = Date.today.to_time
    params[:published] = @today.to_i

    #GET posts by tag or not
    if ( params[:tag])
      @posts = Post.where(
        :'feed_fields.tags' => { "$in" => [params[:tag]]}
      )
      .order_by([[:published, :desc]])
      .page(params[:page])
    end
    if ( params[:by])
      @posts = Post.where(
        :'feed_fields.title' => { "$in" => [params[:by]]}
      )
      .order_by([[:published, :desc]])
      .page(params[:page])
    end
    if( !params[:tag] &&  !params[:by] )
      @posts = Post.all()
        .order_by([[:published, :desc]])
        .page(params[:page])  
    end

    #GET tags
    @tags = Tag.all();
  end


  #hmac_sha1
  def hmac_sha1(data)
      require 'base64'
      require 'cgi'
      require 'openssl'
      secret= ENV['SUPERFEEDER_SECRET']
      hmac = OpenSSL::HMAC.hexdigest(OpenSSL::Digest::Digest.new('sha1'), secret.encode("ASCII"), data.encode("ASCII"))
      return hmac
  end

  def log_feedhook(id, feed, code)
    #Compare sha1 sent in headers with sh1 generated with my secret
    #logger.info hmac
    #logger.info request.headers['HTTP_X_HUB_SIGNATURE']
    #self.request.env.each do |header|
    #  logger.warn "HEADER KEY: #{header[0]}"
    #  logger.warn "HEADER VAL: #{header[1]}"
    #end
    #logger.info params
    
    # logger.tagged("posts feedhook") do
    #   logger.info code
    #   logger.info id
    #   logger.info "#{feed.inspect}"
    # end
  end

  def feedhook
    #logger.info ("PARAMS: #{params.inspect}")
    #delete site prefix "tc_" to get id
    id = params[:id].gsub(Rails.configuration.site[:id]+'_','')
    #hmac = hmac_sha1 ( request.headers['RAW_POST_DATA'] )

    code_initial = params[:status][:code].to_s[0,1]
    logger.info params[:status][:code]

    #if code OK
    if ( code_initial =='2' || code_initial =='3' )
      #get feed associated to this post
      the_feed = Feed.find(id)
      log_feedhook(id, the_feed, params[:status][:code]) #uncomment to log the data in rails logs

      if defined?(params[:items])
        #create post object
        post_j = { 
          :title => params[:items][0][:title], 
          :published => Time.now.to_i,
          :permalinkUrl => params[:items][0][:permalinkUrl]
        }   
        @post = Post.new(post_j)
        @post.feed = the_feed
        
        if ( the_feed.title == 'Test 2') 
          if ( params[:items][0][:title] == 'pok7908')
            #save in DB
            @post.save            
          end
        else
          #save in DB
          @post.save          
        end

      end
    end

    head :ok
  end

end
