class FeedsController < ApplicationController
  before_action :set_feed, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!

  require 'csv'
  require 'uri'


  # REQUIRE LOGGED USER!

  # GET /feeds
  def index
    @feeds = Feed.all
  end


  def healthcheck
    @feeds = Post.collection.aggregate([
    { "$sort" => { "feed_fields.title" => 1, "published" => 1 } },
    { "$group" => {
      "_id"       => "$feed_fields.title",
      "lastDate"  => { "$last" => "$published" },
      "postTitle"     => { "$last": "$title"},
      "postPermalinkUrl"     => { "$last": "$permalinkUrl"}
    }},
    { "$sort" => { "lastDate" => -1 } }
    ])
  end

  def bulk_form
    #insert/update feeds from json in localhost:3000/feeds/bulk_form

    if params[:feeds].present?

      if valid_json?( params[:feeds])
        #iterate json array
        JSON.parse(params[:feeds]).each do |item|

          #create feed
          t_feed = Feed.new({:title=>item['Title'], :url=>item['Url'], :tags => item['Tags'].split(',') })

          #check if feed exists in db
          feed = Feed.where(title: item["Title"]).first

          logger.info "#{feed.inspect}"
          #update if there are changes
          if feed
            Rails.logger.info "IN DB"
            if is_update_needed(feed, t_feed)
              feed.update(title: t_feed.title, url: t_feed.url,tags: t_feed.tags)
              Rails.logger.info "UPDATED"
            end
          else
            Rails.logger.info "CREATED"
            #subscribe to superfeedr & save in db and save posts
            subscribe ( t_feed )
          end

        end

        @params ="OK"
      end
    end

    #respond_to do |format|
    #  format.html { redirect_to feeds_url, notice: 'CSV Feeds were successfully updated.' }
    #end
  end



  # GET /feeds/1
  def show
  end

  # GET /feeds/new
  def new
    @feed = Feed.new
  end

  # GET /feeds/1/edit
  def edit
  end

  # POST /feeds
  def create

    params[:feed][:tags] = params[:feed][:tag_list].split(',')
    params[:feed][:tags] = params[:feed][:tags].collect{|x| x.strip}
    @feed = Feed.new(feed_params)

    respond_to do |format|
      if @feed.save
        format.html { redirect_to @feed, notice: 'Feed was successfully created.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

  # PATCH/PUT /feeds/1
  def update

    params[:feed][:tags] = params[:feed][:tag_list].split(',')
    params[:feed][:tags] = params[:feed][:tags].collect{|x| x.strip}

    respond_to do |format|
      if @feed.update(feed_params)
        format.html { redirect_to @feed, notice: 'Feed was successfully updated.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  # DELETE /feeds/1
  def destroy
    @feed.destroy
    respond_to do |format|
      format.html { redirect_to feeds_url }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_feed
      @feed = Feed.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def feed_params
      params.require(:feed).permit(:title, :url, :id, :tags => [])
    end

  def valid_json?(json)
    begin
      JSON.parse(json)
      return true
    rescue Exception => e
      return false
    end
  end

  def is_update_needed (feed, t_feed)
    if (
      feed.title != t_feed.title ||
      feed.url != t_feed.url ||
      feed.tags != t_feed.tags
      )
      return true
    end
    return false
  end
  
  def get_tags (item)
    #create tags array
    tags = []
    notags=["Title", "Website", "Url"]
    item.each do |property, value|
      if notags.exclude? property and value == 1
        tags.push (property)
      end
    end
    return tags
  end

  def subscribe ( t_feed )

    @result = "result"
    # Configure the middleware with Environment vars in .rben-vars
    Rack::Superfeedr.host = ENV['SUPERFEEDER_HOST']
    Rack::Superfeedr.login = 'earlyriser'
    Rack::Superfeedr.password = 'supermancit0**'
    Rack::Superfeedr.base_path = '/posts/feedhook/'+Rails.configuration.site[:id]+'_'


    Rack::Superfeedr.subscribe(t_feed.url, t_feed.id, {format: "json", secret:ENV['SUPERFEEDER_SECRET'], retrieve: true }) do |body, success, response|

      if success
        flash[:notice] = "Pub/sub OK"
        temp = JSON.parse(body, :quirks_mode => true)
        code_initial = temp["status"]["code"].to_s[0,1]
        @result = body
        logger.info  temp["status"]
        logger.info code_initial

        #if subscribe is ok,
        if ( code_initial =='2' || code_initial =='3' )

          #insert feed in db
          t_feed.save

          #then insert last retrieved posts in db
          temp["items"].each do |item|
            post_j = {
              :title => item["title"],
              :published => item["published"],
              :permalinkUrl => item["permalinkUrl"]
            }
            @post = Post.new(post_j)
            @post.feed = t_feed
            @post.save

          end
        end
      else
        flash[:notice] = "Pub/sub ERROR"
        temp = JSON.parse(body, :quirks_mode => true)
        @result = body
        logger.info  @result
      end
    end
  end



end
