class SettingsController < ApplicationController

  # GET /tags
  # GET /tags.json
  def index
    @feeds = Feed.all.order_by(:title.asc)
    render layout: "application"
  end

end
