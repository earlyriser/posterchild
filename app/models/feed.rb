class Feed
  include Mongoid::Document
  include Mongoid::Alize

  field :title, type: String
  field :url, type: String
  field :tags, type: Array

  has_many :posts 

  def tag_list
    if ( self.tags.kind_of?(Array) )
      self.tags.map { |t| t }.join(", ")
    else
      ""
    end
  end


end
