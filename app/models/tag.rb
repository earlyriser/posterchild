class Tag
  include Mongoid::Document
  field :title, type: String
end
