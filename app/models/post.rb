class Post
  include Mongoid::Document
  include Mongoid::Alize

  field :title, type: String
  field :published, type: String
  field :dateHuman, type: Integer
  field :permalinkUrl, type: String

  belongs_to :feed
  alize :feed

  paginates_per 40 #kaminari pagination
end