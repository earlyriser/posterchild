require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:


require "action_controller/railtie"
require "action_mailer/railtie"
#require "active_resource/railtie" # Comment this line for Rails 4.0+
require "rails/test_unit/railtie"
require "sprockets/railtie" # Uncomment this line for Rails 3.1+


# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module Posterchild
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de


    config.site = {
        name: 'Techtronium',
        id: 'tc',
        domain: 'techtronium.com',
        tagline: 'Startup news, unadulterated',
        shorttagline: 'Startup news',
        twitterUsername: 'techtronium',
        blogUrl: 'http://tumblr.com/techtronium',
        tags: ["Techstars",
            "Y Combinator",
            "Unicorns",
            "AR VR",
            "Accounting",
            "Advertising",
            "Aeronautics",
            "Agriculture",
            "Analytics & Monitoring",
            "Art",
            "Artificial Intelligence",
            "Automation",
            "Banking",
            "Blogging & CMS",
            "Bookmarking",
            "Business intelligence",
            "CRM",
            "Communication & Collaboration",
            "Community",
            "Content",
            "Crowdfunding",
            "Currency",
            "Customer communication",
            "DB/I/P-aaS",
            "Databases",
            "Design",
            "Ecommerce",
            "Edocuments",
            "Education",
            "Electronics",
            "Email & Newsletters",
            "Events",
            "Family",
            "Fashion",
            "Fintech",
            "Fitness & Wellness",
            "Food",
            "Gaming",
            "Geospatial",
            "HR",
            "Health",
            "Home",
            "Investments",
            "Languages",
            "Legal",
            "Logistics",
            "Marketing",
            "Marketplace",
            "Media",
            "Music",
            "Payments & Currency",
            "Photography",
            "Physical product",
            "Productivity",
            "Project Management",
            "Publishing",
            "Q&A",
            "Real estate",
            "Relationships",
            "Retail",
            "Reviews & Recommendations",
            "SaaS",
            "Science",
            "Search",
            "Security",
            "Social Network",
            "Software Development",
            "Sport",
            "Storage",
            "Surveys",
            "Test",
            "Transportation",
            "Travel",
            "Utilities",
            "Video",
            "Web analytics",
            "Websites & Hosting"
          ]
    }

    config.middleware.use Rack::Superfeedr do |superfeedr|
    end
  end
end
