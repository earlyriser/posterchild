# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


tags = Tag.create([
  {title: "Y Combinator"},
  {title: "Techstars"},
  {title: "500 startups"},
  {title: "Marketplace"},
  {title: "SaaS"},
  {title: "Gaming"}
  ])

posts = Post.create([
{ 
  title: "SOMA Water: From $147K on Kickstarter to $2.4M in Funding for the Beautiful Brita Competitor", 
  published: 1417392000,
  permalinkUrl: "http://google.com/blog/abcd", 
  feed_fields: { 
    title: "Mattermark", 
    url: "http://mattermark.com/feed/", 
    tags: [ "Y Combinator", "Finances" ] 
  }   
},
{ feed_fields: { 
    title: "Mattermark", 
    url: "http://mattermark.com/feed/", 
    tags: [ "Y Combinator", "Finances" ] 
  }, 
  title: "Demo Day Is Coming: Ranking the 500 Startups Spring 2013 Batch", 
  published: 1414800000,
  permalinkUrl: "http://google.com/blog/abcd", 
},
{ feed_fields: { 
    title: "Mattermark", 
    url: "http://mattermark.com/feed/", 
    tags: [ "Y Combinator", "Finances" ] 
  }, 
  title: "Mattermark Weekly #1 (Sent Sunday June 9, 2013)", 
  published: 1412121600,
  permalinkUrl: "http://google.com/blog/abcd", 
},
{ feed_fields: { 
    title: "Mattermark", 
    url: "http://mattermark.com/feed/", 
    tags: [ "Y Combinator", "Finances" ] 
  }, 
  title: "Uber Takes on Lyft with Aggressive “Shave the Stache” Mobile Billboard", 
  published: 1409529600,
  permalinkUrl: "http://google.com/blog/abcd", 
},


{ feed_fields: { 
    title: "Google", 
    url: "http://mattermark.com/feed/", 
    tags: [ "Y Combinator", "Search" ] 
  }, 
  title: "'Tis the season for mobile shopping", 
  published: 1417392000, 
  permalinkUrl: "http://google.com/blog/abcd", 
},
{ feed_fields: { 
    title: "Google", 
    url: "http://mattermark.com/feed/", 
    tags: [ "Y Combinator", "Search" ] 
  }, 
  title: "Through the Google lens: search trends November 14-20", 
  published: 1414800000,
  permalinkUrl: "http://google.com/blog/abcd", 
},
{ feed_fields: { 
    title: "Google", 
    url: "http://mattermark.com/feed/", 
    tags: [ "Y Combinator", "Search" ] 
  }, 
  title: "Seven traffic tips to get you to the Thanksgiving table", 
  published: 1412121600,
  permalinkUrl: "http://google.com/blog/abcd", 
},
{ feed_fields: { 
    title: "Google", 
    url: "http://mattermark.com/feed/", 
    tags: [ "Y Combinator", "Search" ] 
  }, 
  title: "Through the Google lens: search trends November 7-13", 
  published: 1409529600,
  permalinkUrl: "http://google.com/blog/abcd", 
},
{ feed_fields: { 
    title: "Google", 
    url: "http://mattermark.com/feed/", 
    tags: [ "Y Combinator", "Search" ] 
  }, 
  title: "The doors are open for veterans at Google", 
  published: 1414807200,
  permalinkUrl: "http://google.com/blog/abcd", 
},


{ feed_fields: { 
    title: "Yahoo", 
    url: "http://mattermark.com/feed/", 
    tags: [ "Y Combinator", "Search" ] 
  }, 
  title: "Yahoo Names Kathy Kayse Vice President, Sales Strategy and Solutions", 
  published: 1417392000,
  permalinkUrl: "http://google.com/blog/abcd", 
},
{ feed_fields: { 
    title: "Yahoo", 
    url: "http://mattermark.com/feed/", 
    tags: [ "Y Combinator", "Search" ] 
  }, 
  title: "Yahoo Reveals 2014’s Most-Searched", 
  published: 1414800000,
  permalinkUrl: "http://google.com/blog/abcd", 
},
{ feed_fields: { 
    title: "Yahoo", 
    url: "http://mattermark.com/feed/", 
    tags: [ "Y Combinator", "Search" ] 
  }, 
  title: "Redecorate with Over 50 Million Photos From Flickr", 
  published: 1412121600,
  permalinkUrl: "http://google.com/blog/abcd", 
},
{ feed_fields: { 
    title: "Yahoo", 
    url: "http://mattermark.com/feed/", 
    tags: [ "Y Combinator", "Search" ] 
  }, 
  title: "Yahoo Mail Launches Beautiful New Stationery with Paperless Post", 
  published: 1414807200,
  permalinkUrl: "http://google.com/blog/abcd", 
},
{ feed_fields: { 
    title: "Yahoo", 
    url: "http://mattermark.com/feed/", 
    tags: [ "Y Combinator", "Search" ] 
  }, 
  title: "Yahoo and Mozilla Partner to Bring Yahoo Search to Firefox", 
  published: 1406851200,
  permalinkUrl: "http://google.com/blog/abcd", 
},


{ feed_fields: { 
    title: "Tumblr", 
    url: "http://mattermark.com/feed/", 
    tags: [ "500 startups", "Media" ] 
  }, 
  title: "Tumblr is so easy to use that it’s hard to explain.", 
  published: 1409529600,
  permalinkUrl: "http://google.com/blog/abcd", 
},
{ feed_fields: { 
    title: "Dribbble", 
    url: "http://mattermark.com/feed/", 
    tags: [ "500 startups", "Media" ] 
  }, 
  title: "DESIGN ON A CAN: PART II", 
  published: 1406851200,
  permalinkUrl: "http://google.com/blog/abcd", 
}

])

